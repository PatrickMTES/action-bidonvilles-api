const nodemailer = require('nodemailer');
const { mail: mailConfig, frontUrl } = require('#server/config');

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    host: mailConfig.smtpHost,
    port: mailConfig.smptPort,
    secure: mailConfig.secure, // true for 465, false for other ports
});

module.exports = {
    generateUserSignature(user) {
        const signature = [
            `${user.first_name} ${user.last_name.toUpperCase()}`,
            `${user.position} - ${user.organization.type.abbreviation || user.organization.name}${user.organization.location.departement !== null ? ` - ${user.organization.location.departement.code}` : ''}`,
            `${user.role} de resorption-bidonvilles.com`,
        ];

        return {
            TextPart: signature.join('\n'),
            HTMLPart: signature.join('<br/>').replace('resorption-bidonvilles.com', `<a href="${frontUrl}">resorption-bidonvilles.com</a>`),
        };
    },

    send(user, mailContent, replyTo = null) {
        // send mail with defined transport object
        return transporter.sendMail({
            from: mailConfig.from, // sender address
            to: user.email, // list of receivers
            subject: mailContent.Subject, // Subject line
            text: mailContent.TextPart, // plain text body
            html: mailContent.HTMLPart, // html body
            replyTo: replyTo !== null ? replyTo.email : undefined,
        });
    },
};
